package ru.kev.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Класс, который создаёт и соединяет
 */
public class TCPConnection {
    private final Socket socket; // сокет для создания соединения
    private final Thread thread;
    private final TCPConnectionListener eventListener;
    private final BufferedReader in;
    private final BufferedWriter out;

    public TCPConnection(TCPConnectionListener eventListener, String ipAddress, int port) throws IOException {
        // создаётся сокет , с заданным ip адресом и портом
        this(eventListener, new Socket(ipAddress,port));
    }

    public TCPConnection(TCPConnectionListener eventListener, Socket socket) throws IOException {
        this.socket = socket;
        this.eventListener = eventListener;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8"))); // ссылка на входной поток сокета
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
        thread = new Thread(new Runnable() { // поток для каждого нового соединения
            @Override
            public void run() {
                try {
                    eventListener.onConnectionReady(TCPConnection.this); // отрабатывает код на активность соединения
                    while (!thread.isInterrupted()){ // пока поток не прерван, пересылаем строчки
                        eventListener.onReceiverString(TCPConnection.this, in.readLine()); // пересылаем строчки ,
                    }
                }catch (IOException e){
                    eventListener.onException(TCPConnection.this, e);
                }finally { // происходит отсоединение , в любом случае будет выполняться
                    eventListener.onDisconnect(TCPConnection.this);
                }
            }
        });
        thread.start();
    }

    public synchronized void sendString(String value) {
        try {
            out.write(value + "\n\r");
            out.flush();
        }catch (IOException e) {
            eventListener.onException(this , e);
        }

    }

    public synchronized void disconnect() {
        thread.interrupt();
        try {
            socket.close();
        }catch (IOException e){
            eventListener.onException(this,e);
        }
    }
    @Override
    public String toString() {
        return "TCPConnection: " +socket.getInetAddress() + ": " +socket.getPort();
    }
}

